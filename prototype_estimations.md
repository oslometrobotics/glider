
Glider notebook
====


```python
import math
import os
from pylab import *
from IPython.display import display, Markdown, Latex
%matplotlib inline
```


```python
# Initialize with specs
internal_radius = 0.035  # Arbitrarily chosen, may be wrong
wall_thickness = 0.003
material_yield_stress = 55e6  # Aluminium 6061
material_density = 2700  # Aluminium 6061
material_young_modulus = 68.9 *1e9  # Aluminium 6061
material_poisson_ratio = 0.33  # Aluminium 6061
internal_volume = 0.0004
min_internal_pressure = 1e5  # We assume 1 bar before compressing
max_external_pressure = 50e5  # 30 bar = 290m depth
min_external_pressure = 1e5  # 1 bar
piston_screw_pitch = 0.005  # Arbitrarily chosen, may be wrong
gear_reduction = 10 # Arbitrarily chosen, may be wrong

```


```python
maximum_pressure_difference = material_yield_stress * wall_thickness / internal_radius
print("Differential pressure: {:2f} Bar".format(maximum_pressure_difference / (1e5)))

displacement_length = internal_volume / (math.pi * internal_radius**2)
print("Cylinder length: {:2f} mm".format(displacement_length * 1000))

wall_transversal_area = ((internal_radius + wall_thickness)**2 - internal_radius**2)
h_0 = displacement_length * 0.1  # h_0 arbitrarily chosen as 10% of displacement length
cylinder_length = displacement_length + h_0
cylinder_wall_weight = math.pi * wall_transversal_area * cylinder_length * material_density

# A very coarse approximation follows
end_cap_thickness = wall_thickness
end_cap_weight = math.pi * (internal_radius + wall_thickness)**2 * end_cap_thickness * material_density
print("Cylinder body_weight: {:2f} kg".format(cylinder_wall_weight + end_cap_weight))

max_internal_pressure = min_internal_pressure * (displacement_length + h_0) / h_0
print("Maximum internal gas pressure: {:2f} bar".format(max_internal_pressure / 1e5))

piston_area = math.pi * internal_radius**2
piston_compression_force = (max_internal_pressure - min_external_pressure) * piston_area
piston_tensile_force = (max_external_pressure - min_internal_pressure) * piston_area
print("Maximum piston compression force: {:2f} N".format(piston_compression_force))
print("Maximum piston tensile force: {:2f} N".format(piston_tensile_force))

# We assume that the maximum engine torque is symmetrical
required_axial_force = max(piston_compression_force, piston_tensile_force)
piston_screw_torque = required_axial_force * piston_screw_pitch / (2*math.pi)
electrical_motor_torque = piston_screw_torque / gear_reduction
print("Electrical motor torque: {:2f} Nm".format(electrical_motor_torque))

critical_radial_pressure = 0.25 * material_young_modulus / (1 - material_poisson_ratio**2) * wall_thickness**3 / (internal_radius + wall_thickness/2.0)**3
critical_axial_pressure = math.pi**2 * material_young_modulus * (internal_radius + wall_thickness/2.0) * wall_thickness / cylinder_length**2
```

    Differential pressure: 47.142857 Bar
    Cylinder length: 103.937922 mm
    Cylinder body_weight: 0.249131 kg
    Maximum internal gas pressure: 11.000000 bar
    Maximum piston compression force: 3848.451001 N
    Maximum piston tensile force: 18857.409903 N
    Electrical motor torque: 1.500625 Nm
    

Buckling
----

![Thin walled cylinder under a pressure difference\label{fig:cylinder_shell}](./images/test_image.png)

Based on the classical thin shell theory [@Timoshenko1961], a simple expression for estimating the buckling load under external pressure yields:

$P_{cr}= \cfrac{1}{4} \cfrac{E}{1-\nu^2} \cfrac{t^3}{r^3}$ = 107.33 bar

_Note that this expression is for long, thin-walled cylinders.
For thick-walled cylinders we would need to use something more sophisticated (like [this link](http://www.engineersedge.com/material_science/pressure_vessel_external_pressure_calculations_9857.htm) or [@Papadakis2008])._

For long cylinders, column buckling is a potential collapse mode, and according to [@Amdahl2005] the buckling pressure can be approximated as

$P_{cr}= \cfrac{\pi^2 E r t}{l^2}$ = 56963.89 bar

As we can observe, axial buckling is not a problem for a micro-glider.

_Note also that we are referring to **external** pressure. Figure \ref{fig:cylinder_shell} is provisory and needs to be updated._

Note: we still haven't accounted for buckling at the cylinder walls or at the end-caps
Link 


<!--


```python
import my_backend
my_backend.generate_docs()
```

    imported
    

-->
\pagebreak \section{References}
