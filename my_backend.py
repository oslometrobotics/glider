from os import system

print("imported")
def generate_docs():
	convert_to_markdown = "jupyter nbconvert --to markdown prototype_estimations.ipynb"
	convert_to_pdf = ' '.join(['pandoc prototype_estimations.md', 
							  '--latex-engine=xelatex', 
							  '-o output/prototype_estimations.pdf',
							  '--filter pandoc-citeproc',
							  '--bibliography=listb.bib',
							  '--csl=format/vancouver.csl'])      								  
	system(convert_to_markdown)                     
	system(convert_to_pdf)