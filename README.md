# Glider prototype estimations

For now this is just a Jupyter notebook with "order-of-magnitude" constructive estimations for building the first glider prototype.

## How to view the notebook

- [This link](https://nbviewer.jupyter.org/urls/bitbucket.org/hioarobotics/glider/raw/master/prototype_estimations.ipynb) displays the last version of the notebook.
- It is also possible to [download a pdf file](https://bitbucket.org/hioarobotics/glider/raw/master/output/prototype_estimations.pdf) with the notebook contents.

## To-do list

- Add annotated images to show what each variable stands for
- ~~Calculate cylindrical wall resistance to buckling~~
- Calculate flat cap (round plate) resistance to buckling
- Dimensioning of O-rings